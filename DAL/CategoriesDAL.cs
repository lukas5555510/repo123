﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class CategoriesDAL : IDAL<CategoryDTO>
    {
        private readonly DataContext _dataContext;
        public CategoriesDAL(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public CategoryDTO Add(CategoryDTO model)
        {

            var d = _dataContext;
            var c = d.Categories;
            var a = c.Add(model);


            CategoryDTO category = a.Entity;
            _dataContext.SaveChanges();
            return category;
        }

        public void Delete(int id)
        {
            _dataContext.Categories.Remove(Get(id));
            _dataContext.SaveChanges();
        }

        public CategoryDTO Get(int id)
        {
            return _dataContext.Categories.Where(x => x.Id == id).FirstOrDefault();
        }

        public List<CategoryDTO> GetAll()
        {
            return _dataContext.Categories.ToList();
        }

        public CategoryDTO Update(CategoryDTO model)
        {
            return _dataContext.Categories.Update(model).Entity;
        }
    }
}
