﻿using DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class DataContext : DbContext
    {
		public DataContext() { }

		public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<CategoryDTO> Categories { get; set; }
        public DbSet<EventDTO> Events { get; set; }
        public DbSet<UserToEventDTO> UsersToEvents { get; set; }

    }
}
