﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class EventsDAL : IDAL<EventDTO>
    {
        private readonly DataContext _dataContext;
        public EventsDAL(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public EventDTO Add(EventDTO model)
        {
            EventDTO eventDTO = _dataContext.Events.Add(model).Entity;
            _dataContext.SaveChanges();
            return eventDTO;
        }

        public void Delete(int id)
        {
            _dataContext.Events.Remove(Get(id));
            _dataContext.SaveChanges();
        }

        public EventDTO Get(int id)
        {
            return _dataContext.Events.Where(x => x.Id == id).FirstOrDefault();
        }

        public List<EventDTO> GetAll()
        {
            return _dataContext.Events.ToList();
        }

        public EventDTO Update(EventDTO model)
        {
            var update = _dataContext.Events.Update(model).Entity;
            _dataContext.SaveChanges();
            return update;
        }
    }
}
