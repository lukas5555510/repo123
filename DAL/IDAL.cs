﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public interface IDAL<T> 
    {
	
        T Get(int id);
        T Add(T model);
        void Delete(int id);
        List<T> GetAll();
        T Update(T model);
    }
}
