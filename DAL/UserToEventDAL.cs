﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class UserToEventDAL : IDAL<UserToEventDTO>
    {
        private readonly DataContext _dataContext;
        public UserToEventDAL(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public UserToEventDTO Add(UserToEventDTO model)
        {
            UserToEventDTO userToeEventDTO = _dataContext.UsersToEvents.Add(model).Entity;
            _dataContext.SaveChanges();
            return userToeEventDTO;
        }

        public void Delete(int id)
        {
            _dataContext.UsersToEvents.Remove(Get(id));
            _dataContext.SaveChanges();
        }

        public UserToEventDTO Get(int id)
        {
            return _dataContext.UsersToEvents.Where(x => x.Id == id).FirstOrDefault();
        }

        public List<UserToEventDTO> GetAll()
        {
            return _dataContext.UsersToEvents.ToList();
        }

        public UserToEventDTO Update(UserToEventDTO model)
        {
            var update = _dataContext.UsersToEvents.Update(model).Entity;
            _dataContext.SaveChanges();
            return update;
        }
    }
}
