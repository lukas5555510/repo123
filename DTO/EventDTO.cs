﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class EventDTO
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public DateTime MeetingDate { get; set; }
        public string Name { get; set; }
        public int MaxQuantity { get; set; }
        public int ActualQuantity { get; set; }
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public string Person_Details { get; set; }




    }
}
