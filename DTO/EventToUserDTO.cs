﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class UserToEventDTO
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public string UserId { get; set; }

    }
}
