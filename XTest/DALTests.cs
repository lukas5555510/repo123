﻿
using AutoFixture;
using DAL;
using DTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace XTest
{
    public class DALTests
    {
        [Theory]
        [InlineData(1, "oooooooo", "iiiiiii")]
        [InlineData(2324224, "fghfghfghfgh", "hhhhhhh")]
        [InlineData(3, "yyyyyyyyyyy", "tttttttttt")]
        [InlineData(4, "kljkhgfgfgh", "12345678zaqzaqzaq")]
        [InlineData(18928, "różne daneeee 44566", "Jest Dobrze")]
        [InlineData(111111, "różne dane 123", "różne dane 357")]
        public void EventsDAL_Add(int Id, string Name, string Description)
        {
            IDAL<EventDTO> sut = _GetInMemoryPersonRepository();
            EventDTO _event = new EventDTO();

            _event.Id = Id;
            _event.Name = Name;
            _event.Description = Description;

            EventDTO savedCategory = sut.Add(_event);


            Assert.Equal(_event.Id, savedCategory.Id);
            Assert.Equal(_event.Name, savedCategory.Name);
            Assert.NotEmpty(savedCategory.Description);
        }

        [Theory]
        [InlineData(1, "oooooooo", "iiiiii")]
        [InlineData(2324224, "fghfghfghfgh", "hhhhhh")]
        [InlineData(3, "yyyyyyyyyyy", "ttttttttt")]
        [InlineData(4, "kljkhgfgfgh", "12345678zaqzqzaq")]
        [InlineData(18928, "różne daneeee 44566", "Jest Dobrze")]
        [InlineData(111111, "różne dane 123", "różne dane 37")]
        public void EventsDAL_Update(int Id, string Name, string Description)
        {
            IDAL<EventDTO> sut = _GetInMemoryPersonRepository();
            EventDTO _event = new EventDTO();
            _event.Id = Id;
            _event.Name = Name;
            _event.Description = Description;
                        
            EventDTO eventDTO = sut.Add(_event);
            EventDTO savedCategory = sut.Update(_event);

            Assert.NotEmpty(savedCategory.Description);

        }

        [Theory]
        [InlineData(1, "oooooooo", "iiiiiii")]
        [InlineData(2324224, "fghfghfghfgh", "hhhhhhh")]
        [InlineData(3, "yyyyyyyyyyy", "tttttttttt")]
        [InlineData(4, "kljkhgfgfgh", "12345678zaqzaqzaq")]
        [InlineData(18928, "różne daneeee 44566", "Jest Dobrze")]
        [InlineData(111111, "różne dane 123", "różne dane 357")]
        public void EventsDAL_Get(int Id, string Name, string Description)
        {
            IDAL<EventDTO> sut = _GetInMemoryPersonRepository();
            EventDTO _event = new EventDTO();
            _event.Id = Id;
            _event.Name = Name;
            _event.Description = Description;

            EventDTO savedCategory = sut.Add(_event);
            EventDTO categoryDTO = sut.Get(savedCategory.Id);
            Assert.Equal(savedCategory.Id, categoryDTO.Id);
            Assert.Equal(savedCategory.Name, categoryDTO.Name);
            Assert.Equal(savedCategory.Description, categoryDTO.Description);
        }

        [Theory]
        [InlineData(1, "oooooooo", "iiiiiii")]
        [InlineData(2324224, "fghfghfghfgh", "hhhhhhh")]
        [InlineData(3, "yyyyyyyyyyy", "tttttttttt")]
        [InlineData(4, "kljkhgfgfgh", "12345678zaqzaqzaq")]
        [InlineData(18928, "różne daneeee 44566", "Jest Dobrze")]
        [InlineData(111111, "różne dane 123", "różne dane 357")]
        public void EventsDAL_GetAll(int Id, string Name, string Description)
        {
            IDAL<EventDTO> sut = _GetInMemoryPersonRepository();
            EventDTO _event = new EventDTO();
            _event.Id = Id;
            _event.Name = Name;
            _event.Description = Description;

            EventDTO savedCategory = sut.Add(_event);
            List<EventDTO> list = sut.GetAll();

            Assert.True(list.Contains(savedCategory));
        }


        [Theory]
        [InlineData(1, "oooooooo", "iiiiiii")]
        [InlineData(2324224, "fghfghfghfgh", "hhhhhhh")]
        [InlineData(3, "yyyyyyyyyyy", "tttttttttt")]
        [InlineData(4, "kljkhgfgfgh", "12345678zaqzaqzaq")]
        [InlineData(18928, "różne daneeee 44566", "Jest Dobrze")]
        [InlineData(111111, "różne dane 123", "różne dane 357")]
        public void EventsDAL_Delete(int Id, string Name, string Description)
        {
            IDAL<EventDTO> sut = _GetInMemoryPersonRepository();
            EventDTO _event = new EventDTO();
            _event.Id = Id;
            _event.Name = Name;
            _event.Description = Description;


            EventDTO savedCategory = sut.Add(_event);
            sut.Delete(savedCategory.Id);
            EventDTO categoryDTO = sut.Get(savedCategory.Id);

            Assert.Null(categoryDTO);
        }
        //-------------------------------------------------------------------------------

        [Theory]
        [InlineData(1, "oooooooo", "iiiiiii")]
        [InlineData(2324224, "fghfghfghfgh", "hhhhhhh")]
        [InlineData(3, "yyyyyyyyyyy", "tttttttttt")]
        [InlineData(4, "kljkhgfgfgh", "12345678zaqzaqzaq")]
        [InlineData(18928, "różne daneeee 44566", "Jest Dobrze")]
        [InlineData(111111, "różne dane 123", "różne dane 357")]
        public void CategoriesDAL_Add(int Id, string Name, string Description)
        {
			IDAL<CategoryDTO> sut = GetInMemoryPersonRepository();
            CategoryDTO category = new CategoryDTO();

            category.Id = Id;
            category.Name = Name;
            category.Description = Description;

            CategoryDTO savedCategory = sut.Add(category);

		
			Assert.Equal(category.Id, savedCategory.Id);
			Assert.Equal(category.Name, savedCategory.Name);
			Assert.NotEmpty(savedCategory.Description);
		}

        [Theory]
        [InlineData(1,"oooooooo","iiiiiii")]
        [InlineData(2324224, "fghfghfghfgh", "hhhhhhh")]
        [InlineData(3, "yyyyyyyyyyy", "tttttttttt")]
        [InlineData(4, "kljkhgfgfgh", "12345678zaqzaqzaq")]
        [InlineData(18928, "różne daneeee 44566", "Jest Dobrze")]
        [InlineData(111111, "różne dane 123", "różne dane 357")]
        public void CategoriesDAL_Update(int Id, string Name,string Description)
        {
            IDAL<CategoryDTO> sut = GetInMemoryPersonRepository();
            CategoryDTO category = new CategoryDTO();
            category.Id = Id;
            category.Name = Name;
            category.Description = Description;            

            CategoryDTO savedCategory = sut.Update(category);
            
            Assert.NotEmpty(savedCategory.Description);
            
        }

        [Theory]
        [InlineData(1, "oooooooo", "iiiiiii")]
        [InlineData(2324224, "fghfghfghfgh", "hhhhhhh")]
        [InlineData(3, "yyyyyyyyyyy", "tttttttttt")]
        [InlineData(4, "kljkhgfgfgh", "12345678zaqzaqzaq")]
        [InlineData(18928, "różne daneeee 44566", "Jest Dobrze")]
        [InlineData(111111, "różne dane 123", "różne dane 357")]
        public void CategoriesDAL_Get(int Id, string Name, string Description)
        {
            IDAL<CategoryDTO> sut = GetInMemoryPersonRepository();
            CategoryDTO category = new CategoryDTO();
            category.Id = Id;
            category.Name = Name;
            category.Description = Description;

            CategoryDTO savedCategory = sut.Add(category);            
            CategoryDTO categoryDTO = sut.Get(savedCategory.Id);
            Assert.Equal(savedCategory.Id,categoryDTO.Id);
            Assert.Equal(savedCategory.Name, categoryDTO.Name);
            Assert.Equal(savedCategory.Description, categoryDTO.Description);
        }

        [Theory]
        [InlineData(1, "oooooooo", "iiiiiii")]
        [InlineData(2324224, "fghfghfghfgh", "hhhhhhh")]
        [InlineData(3, "yyyyyyyyyyy", "tttttttttt")]
        [InlineData(4, "kljkhgfgfgh", "12345678zaqzaqzaq")]
        [InlineData(18928, "różne daneeee 44566", "Jest Dobrze")]
        [InlineData(111111, "różne dane 123", "różne dane 357")]
        public void CategoriesDAL_GetAll(int _Id, string _Name, string _Description)
        {
            IDAL<CategoryDTO> sut = GetInMemoryPersonRepository();
            CategoryDTO category = new CategoryDTO();
            category.Id = _Id;
            category.Name = _Name;
            category.Description = _Description;

            CategoryDTO savedCategory = sut.Add(category);
            List<CategoryDTO> list = sut.GetAll();

            Assert.True(list.Contains(savedCategory));
        }


        [Theory]
        [InlineData(1, "oooooooo", "iiiiiii")]
        [InlineData(2324224, "fghfghfghfgh", "hhhhhhh")]
        [InlineData(3, "yyyyyyyyyyy", "tttttttttt")]
        [InlineData(4, "kljkhgfgfgh", "12345678zaqzaqzaq")]
        [InlineData(18928, "różne daneeee 44566", "Jest Dobrze")]
        [InlineData(111111, "różne dane 123", "różne dane 357")]
        public void CategoriesDAL_Delete(int Id, string Name, string Description)
        {
            IDAL<CategoryDTO> sut = GetInMemoryPersonRepository();
            CategoryDTO category = new CategoryDTO();
            category.Id = Id;
            category.Name = Name;
            category.Description = Description;


            CategoryDTO savedCategory = sut.Add(category);
            sut.Delete(savedCategory.Id);
            CategoryDTO categoryDTO = sut.Get(savedCategory.Id);
          
            Assert.Null(categoryDTO); 
        }

        private IDAL<CategoryDTO> GetInMemoryPersonRepository()
		{
			DbContextOptions<DataContext> options;
			var builder = new DbContextOptionsBuilder<DataContext>();
			builder.UseInMemoryDatabase();
			options = builder.Options;
			DataContext dataContext = new DataContext(options);
			dataContext.Database.EnsureDeleted();
			dataContext.Database.EnsureCreated();
			dataContext.Categories.AddRange(AddTestData<CategoryDTO>());
			dataContext.Events.AddRange(AddTestData<EventDTO>());
			return new CategoriesDAL(dataContext);
		}

        private IDAL<EventDTO> _GetInMemoryPersonRepository()
        {
            DbContextOptions<DataContext> options;
            var builder = new DbContextOptionsBuilder<DataContext>();
            builder.UseInMemoryDatabase();
            options = builder.Options;
            DataContext dataContext = new DataContext(options);
            dataContext.Database.EnsureDeleted();
            dataContext.Database.EnsureCreated();
            dataContext.Categories.AddRange(AddTestData<CategoryDTO>());
            dataContext.Events.AddRange(AddTestData<EventDTO>());
            return new EventsDAL(dataContext);
        }

        private List<T> AddTestData<T>()
		{
			Fixture _fixture = new Fixture();
			return _fixture.CreateMany<T>(20).ToList();
		}
	}
}
