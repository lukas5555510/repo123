﻿using DAL;
using DTO;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace zbierzekipepl.Config
{
    public static class BindingService
    {
        public static void Bind(IServiceCollection services)
        {
            services.AddTransient<IDAL<CategoryDTO>, CategoriesDAL>();
            services.AddTransient<IDAL<EventDTO>, EventsDAL>();
        }
    }
}
