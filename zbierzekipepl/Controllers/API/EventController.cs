﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using zbierzekipepl.Models;

namespace zbierzekipepl.Controllers
{

    [Produces("application/json")]
    [Route("api/Event/[action]")]
    public class EventController : Controller
    {
        public readonly IDAL<EventDTO> _eventsDAL;

        public EventController (IDAL<EventDTO> eventDAL)
        {
            _eventsDAL = eventDAL;
        }

        [HttpGet]
        public IActionResult GetEvent(int id)
        {
            try
            {
                var even = _eventsDAL.Get(id);
                if (even != null)
                {
                   return Ok(even);
                }
                else
                {
                    return NotFound("sorry");
                }
            } catch(Exception e)
            {
                return Ok("sorry");
            }
        }

        [HttpGet]
        public IActionResult GetAllEvents()
        {
            try
            {
                var events = _eventsDAL.GetAll();
                if (events != null)
                {
                    return Ok(events);
                }
                else
                {
                    return NotFound("sorry");
                }
            }
            catch (Exception e)
            {
                return Ok("sorry");
            }
        }

        [HttpPost]
        public IActionResult AddEvent(EventModel model)
        {
            EventDTO eventDTO = new EventDTO();

            eventDTO.Location = model.Location;
            eventDTO.MaxQuantity = model.MaxQuantity;
            eventDTO.MeetingDate = model.MeetingDate;
            eventDTO.Name = model.Name;
            eventDTO.ActualQuantity = model.ActualQuantity;
            eventDTO.CategoryId = model.CategoryId;
            eventDTO.Description = model.Description;
            eventDTO.Person_Details = model.Person_Details;

            try
            {
                var even = _eventsDAL.Add(eventDTO);
                if (even != null)
                {
                    return Ok(even);
                }
                else
                {
                    return NotFound("sorry");
                }
            }
            catch (Exception e)
            {
                return Ok("sorry");
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult SubscribeToEvent(int id)
        {
           var currentUser = HttpContext.User;
           var even = _eventsDAL.Get(id);
            if (even.ActualQuantity < even.MaxQuantity)
            {
                even.ActualQuantity++;
                _eventsDAL.Update(even);
            }
            return Ok(even);
        }

        [Authorize]
        [HttpPost]
        public IActionResult UnSubscribeToEvent(int id)
        {
            var even = _eventsDAL.Get(id);
            if (even.ActualQuantity > 0)
            {
                even.ActualQuantity--;
                _eventsDAL.Update(even);
            }
            return Ok(even);
        }

    }
}