﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using DTO;
using Microsoft.AspNetCore.Mvc;
using zbierzekipepl.Models;

namespace zbierzekipepl.Controllers
{

    [Produces("application/json")]
    [Route("api/Event/[action]")]
    public class CategoryController : Controller
    {
        public readonly IDAL<CategoryDTO> _categoriesDAL;

        public CategoryController(IDAL<CategoryDTO> categoryDAL)
        {
            _categoriesDAL = categoryDAL;
        }

        [HttpGet]
        public IActionResult GetCatergory(int id)
        {
            try
            {
                var category = _categoriesDAL.Get(id);
                if (category != null)
                {
                    return Ok(category);
                }
                else
                {
                    return NotFound("sorry");
                }
            }
            catch (Exception c)
            {
                return Ok("sorry");
            }
        }

            [HttpGet]
        public IActionResult GetAllCategories()
        {
            try
            {
                var categories = _categoriesDAL.GetAll();
                if (categories != null)
                {
                    return Ok(categories);
                }
                else
                {
                    return NotFound("sorry");
                }
            }
            catch (Exception c)
            {
                return Ok("sorry");
            }
        }

        [HttpPost]
        public IActionResult AddCategory(CategoryModel model)
        {
            CategoryDTO categoryDTO = new CategoryDTO();

            categoryDTO.Description = model.Description;
            categoryDTO.Name = model.Name;


            try
            {
                var category = _categoriesDAL.Add(categoryDTO);
                if (category != null)
                {
                    return Ok(category);
                }
                else
                {
                    return NotFound("sorry");
                }
            }
            catch (Exception c)
            {
                return Ok("sorry");
            }
        }

    }
}
