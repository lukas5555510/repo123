﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using DTO;
using Microsoft.AspNetCore.Mvc;

namespace zbierzekipepl.Controllers
{
    public class EventsController : Controller
    {
        public readonly IDAL<EventDTO> _eventsDAL;

        public EventsController(IDAL<EventDTO> eventDAL)
        {
            _eventsDAL = eventDAL;
        }

        public IActionResult Index()
        {
            List<EventDTO> list = _eventsDAL.GetAll();
            return View(list);
        }
    }
}