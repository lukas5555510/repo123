﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using DTO;
using Microsoft.AspNetCore.Mvc;
using zbierzekipepl.Models;

namespace zbierzekipepl.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : Controller
    {
        public readonly IDAL<CategoryDTO> _categoryDAL;

        public HomeController(IDAL<CategoryDTO> categoryDAL)
        {
            _categoryDAL = categoryDAL;
        }

        public IActionResult Index()
        {
            var model = _categoryDAL.Get(0);
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
