﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace zbierzekipepl.Models
{
    public class CategoryModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
